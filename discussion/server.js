//[SECTION] Creating A Basic Server Setup

//Node JS => Will provide us with a "runtime environment (RTE)" that will allow us to execute our program/application. 

//RTE (Runtime Environment) -> we are pertaining to an environment/system in which a program can be executed. 

//TASK: let's create a standard server setup using 'plain' NodeJS. 

//1. Identify and prepare the components/Ingredients that you would need in order to execute the task. 

     //the main ingredient when creating a server using plain Node is:  *http* (hypertext transfet protocol). 
     //http -> is a 'built in' module of NODE JS, that will allow us to 'establish a connnection' and will make it possible to transfer data over HTTP. 

     //you need to be able to get the components first, 

     //we will use the require() directive -> this is a function that will allow us to gather and acquire certain packages that we will use to build our application.  
const http = require('http'); 
     //http will provide us with all the components needed to establish a server.   

     //http contains the utility constructor called 'createServer()' -> which will allow us to create an HTTP server. 

//2. Identify and describe a location where the connection will happen. in order to provide a proper medium for both parties (client and server), we will then bind the connection to the desired port number. 
let port = 4000; 

//3. Inside the server constructor, insert a method in which we can use in order describe the connect that was established. identify the interaction between the client and the server and pass them down as the arguments of the method.


http.createServer((request, response) => {

 //4. bind/assign the connection to the address, the listen() is used to bind and listen to a specific port whenever its being access by the computer. 

 //write() -> this will allow to insert messages or input to our page. 
   response.write(`Maligayang Bati to Port ${port}`); 
   response.end(); //end() will allow us to identify a point where the transmission of data will end. 
}).listen(port); 

console.log('Server is Running'); 


//NOTE: when creating project, we should always think of more efficient ways in executing even the most smallest task. 

//TASK: Intergrate the use of an NPM Project in Node JS. 

  //1. INITALIZE an NPM(Node Package Manager) into the local project. 

      //(2 ways to perform this task)

      //METHOD 1: npm init 

      //METHOD 2: npm init -y(yes)

      //package.json => 'The heart of any Node projects'. it records all the important metadata about the project, (libraries, packages, function) that makes up the system or application. 

      //2 mins (send a screenshot of your package.json) in HO

      //Now, that we have integrated an NPM into our project, lets solve some issues we are facing in our local project. 

        //issue: needing to restart the project whenever changes occur.

        //solve: using NPM lets install a dependency that will allow to automatically fix (hotfix) changes in our app. 

        //=> 'nodemon' package

        //BASIC SKILLS using NPM

        //-> install package
          //1st method: npm install <name of packages>
          //2nd method: npm i <name of packages>

          //note: you can also install multiple packages at the same time. 
             //nodemon
             //express
             //bcrypt

        //-> uninstall or remove a package
           //1st method: npm uninstall <name of package>
           //2nd method: npm un <name of package>

        //nodemon utility => is a CLI utility tool, it's task is to wrap your node js app and watch for any changes in the file system and automatically restart/hotfic the process. 

        //IF this is the first time you local machine would us a platform/technology, the machine would need to recognize the technology first

        //ISSUE: command not found

        //YOU INSTALL IT ON A 'GLOBAL' SCALE. 
        //'GLOBAL' => the file system structure of the whole machine.
        //npm install -g nodemon 

        //After installing the new package, register it to the package.json file.
